import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/simple",
    name: "Simple",
    // route level code-splitting
    // this generates a separate chunk (simple.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/Simple.vue")
  },
  {
    path: "/air",
    name: "Air",
    component: () => import("../views/Air.vue")
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
